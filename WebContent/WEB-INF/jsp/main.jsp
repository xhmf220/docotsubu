<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.User,model.Mutter,java.util.List" %>
<%
User loginUser = (User) session.getAttribute("loginUser");

List<Mutter> mutterList = (List<Mutter>) request.getAttribute("mutterList");

String errorMsg = (String) request.getAttribute("errorMsg");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Noto+Sans+JP:400,700" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
.site-container{ text-align:center; font-size:3rem  color:black; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px;
 }
.post{
 margin: 7px;padding: 7px;
 background: -webkit-radial-gradient(rgb(239, 241, 255), rgb(255, 255, 255));
 background: radial-gradient(rgb(239, 241, 255), rgb(255, 255, 255));
   font-family: 'Lato', 'Noto Sans JP', '游ゴシック Medium', '游ゴシック体', 'Yu Gothic Medium', YuGothic, 'ヒラギノ角ゴ ProN', 'Hiragino Kaku Gothic ProN', 'メイリオ', Meiryo, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif; font-size:1.3rem; border:ridge 2px lightblue;}
.log{margin: 7px;padding: 7px;font-family: 'Lato', 'Noto Sans JP', '游ゴシック Medium', '游ゴシック体', 'Yu Gothic Medium', YuGothic, 'ヒラギノ角ゴ ProN', 'Hiragino Kaku Gothic ProN', 'メイリオ', Meiryo, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;}
.mutterform{width:300px; height:120px;margin: 7px;padding: 7px;}
.logObtn{background-color:#6495ED; color:white;}
.reply{font-size:1.2rem}
.kousin{font-size:1.5rem;margin: 7px;padding: 7px;}


</style>




<title>どこつぶ</title>
</head>
<body style="background-image: url(image/mizutama.jpg)">

<div class="site-container container">
  <h1>DOCOTSUBU</h1>
</div>

<div class="log">
<p><%= loginUser.getName() %>さん、ログイン中 <a href="/docoTsubu/Logout"><button type="button" class="logObtn">ログアウト</button></a>
</p></div>



<form action="/docoTsubu/Main" method="post">
<input type="text" name="text" class="mutterform">
<input type="submit" class="btn btn-primary" value="投稿" >
</form><br>
<div class="kousin"><p><a href="/docoTsubu/Main"><i class="fas fa-redo-alt"></i></a></p></div>

<% if(errorMsg != null) { %>
<p><%= errorMsg %></p>
<% }%>


<% for(Mutter mutter : mutterList) {%>
	<div class="post">
	<p><%= mutter.getUserName() %>：<%= mutter.getText() %></p>
	<p id="replyText"></p>
	<div class="reply"><a onClick="reply()"><i class="fas fa-reply"></i></a>
	</div>
	</div>

<% } %>

<script>
	function reply(){
 		let text= window.prompt("返信内容を入力","");
 		if(text != null){
 			document.getElementById("replyText").innerHTML = ">>"+text;
 		}
	}
</script>

</body>
</html>